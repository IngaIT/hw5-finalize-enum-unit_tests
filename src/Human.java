import java.util.Arrays;
import java.util.Objects;

public class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private DaySchedule[] schedule;
    private Family family;

    public Human() {}

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, int year, int iq, DaySchedule[] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
    }

    public enum DaySchedule {
        FRIDAY_THEATRE, MONDAY_WORK, SATURDAY_SWIMMING, SUNDAY_SLEEP, THURSDAY_CINEMA, TUESDAY_WORK, WEDNESDAY_STUDY
    }

    public void greetPet() {
        System.out.println("Привіт, " + family.getPet().getNickname());
    }

    public void describePet() {
        System.out.println("У мене є " + family.getPet().getSpecies() + ", їй " + family.getPet().getAge() + " років, він " + (family.getPet().getTrickLevel() > 50 ? "дуже хитрий" : "майже не хитрий"));
    }

    @Override
    public String toString() {
        return "Human{name='" + name + "', surname='" + surname + "', year=" + year + ", iq=" + iq + ", schedule=" + Arrays.toString(schedule) + "}";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Human other = (Human) obj;
        return year == other.year &&
                iq == other.iq &&
                Objects.equals(name, other.name) &&
                Objects.equals(surname, other.surname) &&
                Arrays.equals(schedule, other.schedule);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(name, surname, year, iq);
        result = 31 * result + Arrays.hashCode(schedule);
        return result;
    }


    @Override
    protected void finalize() throws Throwable {
        System.out.println(this.toString() + " is being deleted by the Garbage Collector");
        super.finalize();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public DaySchedule[] getSchedule() {
        return schedule;
    }

    public void setSchedule(DaySchedule[] schedule) {
        this.schedule = schedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }
}
