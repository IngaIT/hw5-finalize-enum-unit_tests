import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FamilyTest {

    @Test
    public void testDeleteChild_ValidIndex_ReturnsTrueAndRemovesChild() {
        Human father = new Human("Олег", "Іванов", 1975);
        Human mother = new Human("Олена", "Іванова", 1977);
        Human child1 = new Human("Марія", "Іванова", 2001);
        Human child2 = new Human("Іван", "Іванов", 2005);
        Family family = new Family(mother, father);
        family.addChild(child1);
        family.addChild(child2);

        boolean result = family.deleteChild(1);
        Assertions.assertTrue(result);
        Assertions.assertEquals(1, family.getChildren().length);
        Assertions.assertEquals(child1, family.getChildren()[0]);
    }

    @Test
    public void testDeleteChild_InvalidIndex_ReturnsFalseAndDoesNotChangeChildren() {
        Human father = new Human("Олег", "Іванов", 1975);
        Human mother = new Human("Олена", "Іванова", 1977);
        Human child1 = new Human("Марія", "Іванова", 2001);
        Human child2 = new Human("Іван", "Іванов", 2005);
        Family family = new Family(mother, father);
        family.addChild(child1);
        family.addChild(child2);

        boolean result = family.deleteChild(2);
        Assertions.assertFalse(result);
        Assertions.assertEquals(2, family.getChildren().length);
        Assertions.assertEquals(child1, family.getChildren()[0]);
        Assertions.assertEquals(child2, family.getChildren()[1]);
    }

    @Test
    public void testAddChild_IncreasesChildrenArrayAndAddsChild() {
        Human father = new Human("Олег", "Іванов", 1975);
        Human mother = new Human("Олена", "Іванова", 1977);
        Family family = new Family(mother, father);
        Human child = new Human("Марія", "Іванова", 2001);

        family.addChild(child);

        Assertions.assertEquals(1, family.getChildren().length);
        Assertions.assertEquals(child, family.getChildren()[0]);
    }

    @Test
    public void testCountFamily_ReturnsCorrectNumberOfPeople() {
        Human father = new Human("Олег", "Іванов", 1975);
        Human mother = new Human("Олена", "Іванова", 1977);
        Human child1 = new Human("Марія", "Іванова", 2001);
        Human child2 = new Human("Іван", "Іванов", 2005);
        Family family = new Family(mother, father);
        family.addChild(child1);
        family.addChild(child2);

        int result = family.countFamily();
        Assertions.assertEquals(4, result);
    }
}
